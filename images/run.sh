#!/bin/bash

RABBITMQ=$(docker run -d --name rabbitmq dockerfile/rabbitmq:latest)
DB=$(docker run -d --name db training/postgres:latest)
API=$(docker run -d --name nds-api --link db:db --link rabbitmq:rabbitmq -p 80 ndslabs/nds_django:api)

NUMWORKERS=3
WORKERS=()

for i in $(seq 1 $NUMWORKERS)
do
  ID=$(docker run -d --privileged --name nds-worker-$i --link db:db --link rabbitmq:rabbitmq ndslabs/nds_django:worker)
  WORKERS+=($ID)
done

PORT=$(docker inspect $API | grep HostPort | grep -v \"\" | awk '{print $2}' | sed 's/\"//g')


echo "RabbitMQ container ID: $RABBITMQ"
echo "Postgres container ID: $DB"
echo "HTTP API container ID: $API"
echo "Workers IDs:"
for i in ${WORKERS[@]}
do
  echo "    $i"
done
echo "Connect to http://127.0.0.1:$PORT"
