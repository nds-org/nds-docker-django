Run ```mk_images.sh``` to build the docker containers, or pull ```ndslabs/nds_rest_api:latest``` and ```ndslabs/nds_rest_worker:latest``` from the docker hub.



```bash
$ docker run -d --name rabbitmq dockerfile/rabbitmq:latest
$ docker run -d --name db training/postgres:latest
$ docker run -d --name nds-api --link db:db --link rabbitmq:rabbitmq -p 80 ndslabs/nds_rest_api:latest
$ docker run -d --privileged --name nds-worker1 --link db:db --link rabbitmq:rabbitmq ndslabs/nds_rest_worker:latest
$ docker run -d --privileged --name nds-worker2 --link db:db --link rabbitmq:rabbitmq ndslabs/nds_rest_worker:latest
$ docker run -d --privileged --name nds-worker3 --link db:db --link rabbitmq:rabbitmq ndslabs/nds_rest_worker:latest
```
