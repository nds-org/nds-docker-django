#!/bin/bash

rm -rf build
git clone git@bitbucket.org:nds-org/nds-django.git build
pushd build &> /dev/null
git submodule init
git submodule update
pushd ythub_workers &> /dev/null
git pull origin master
popd &> /dev/null
popd &> /dev/null

docker build -t ndslabs/nds_django:worker .
