#!/bin/bash

for i in * ; do
  if [ -d "$i" ]; then
    echo "Building image in $i"
    pushd $i &> /dev/null
    ./mk_image.sh
    popd &> /dev/null
  fi
done

echo "Done"
